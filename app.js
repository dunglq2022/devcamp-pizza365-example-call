//Khai báo express
const express = require('express');

//Khai báo path
const path = require('path');

//Khai báo port
const port = 8000;

//Tạo app
const app = express();

app.use('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/view/index.html'));
})

//Listen app
app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
})